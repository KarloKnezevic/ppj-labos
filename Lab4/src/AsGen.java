import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Stack;


/**
 * Generiranje asemblerskih naredbi
 * @author wex
 *
 */
public class AsGen {
	private static int jpLabelCnt = 0;
	private static int loopCnt = 0;
	private static Stack<Integer> loops = new Stack<Integer>();
	
	public static String getJumpLabel(){
		return "JP_LABEL_" + Integer.valueOf(jpLabelCnt++).toString() + "\n";
	}
	
	public static int getNewLoopID(){
		loops.push(loopCnt);
		return loopCnt++;
	}
	
	public static int peekLoopID(){
		return loops.peek();
	}
	
	public static void leaveLoop(){
		loops.pop();
	}
	
	public static void addAssembler(String comm){
		if(Analyzator.curFunction != null)
			Analyzator.curFunction.code.append(comm);
		else
			Analyzator.codeFRISC.append(comm);
	}
	
	public static void finishAssembly() throws FileNotFoundException{
		Analyzator.codeFRISC.insert(0, " MOVE 40000, R7\n");
		Analyzator.codeFRISC.append(" CALL F_main\n HALT\n");
		
		for(FunctionFRISC cur : Analyzator.functionsFRISC.values())
			Analyzator.codeFRISC.append(cur.label + cur.code);
		
		int nrLine = Analyzator.codeFRISC.toString().split("\n").length;
		nrLine += Analyzator.varsFRISC.size();
		
		for(VarFRISC cur : Analyzator.varsFRISC){
			if(cur.initValue == null){
				cur.initValue = Integer.valueOf(4*nrLine).toString();
				nrLine += cur.arraySize;
			}
			Analyzator.codeFRISC.append(cur.toString() + "\n");
		}
		
		Analyzator.codeFRISC.append(Analyzator.globalArrays);
		
		addAdditionalLibrary(); //dodaje potprogram za mnozenje, dijeljenje i modulo
		
		PrintWriter writer = new PrintWriter("./a.frisc");
		writer.print(Analyzator.codeFRISC);
		writer.close();
		
		//System.out.println(Analyzator.codeFRISC.toString()); //odkomentiraj ovo da ispis bude i na stdout
	}
	
	public static String load(String o1, String o2){
		return " LOAD " + o1 + ", " + o2 + "\n";
	}
	
	public static String store(String o1, String o2){
		return " STORE " + o1 + ", " + o2 + "\n";
	}
	
	public static String pop(String o1){
		return " POP " + o1 + "\n";
	}
	
	public static String push(String o1){
		return " PUSH " + o1 + "\n";
	}
	
	public static String move(String o1, String o2){
		return " MOVE " + o1 + ", " + o2 + "\n";
	}
	
	public static String ret(){
		return " RET\n";
	}
	
	public static String xor(String o1, String o2, String o3){
		return " XOR " + o1 + ", " + o2 + ", " + o3 + "\n";
	}
	
	public static String add(String o1, String o2, String o3){
		return " ADD " + o1 + ", " + o2 + ", " + o3 + "\n";
	}
	
	public static String cmp(String o1, String o2){
		return " CMP " + o1 + ", " + o2 + "\n";
	}
	
	public static String jp(String cond, String o1){
		if(cond != null)
			return " JP_" + cond + " " + o1 + "\n";
		else
			return " JP " + o1 + "\n";
	}
	
	public static String sub(String o1, String o2, String o3){
		return " SUB " + o1 + ", " + o2 + ", " + o3 + "\n";
	}
	
	public static String or(String o1, String o2, String o3){
		return " OR " + o1 + ", " + o2 + ", " + o3 + "\n";
	}
	
	public static String and(String o1, String o2, String o3){
		return " AND " + o1 + ", " + o2 + ", " + o3 + "\n";
	}
	
	public static String call(String fname){
		return " CALL " + "F_" + fname + "\n";
	}
	
	public static String shl(String o1, String o2, String o3){
		return " SHL " + o1 + ", " + o2 + ", " + o3 + "\n";
	}
	
	private static void addAdditionalLibrary(){
		Analyzator.codeFRISC.append("MD_SGN MOVE 0, R6\n XOR R0, 0, R0\n JP_P MD_TST1\n XOR R0, -1, R0\n ADD R0, 1, R0\n MOVE 1, R6");
		Analyzator.codeFRISC.append("\nMD_TST1 XOR R1, 0, R1\n JP_P MD_SGNR\n XOR R1, -1, R1\n ADD R1, 1, R1\n XOR R6, 1, R6");
		Analyzator.codeFRISC.append("\nMD_SGNR RET\nMD_INIT POP R4\n POP R3\n POP R1\n POP R0\n CALL MD_SGN");
		Analyzator.codeFRISC.append("\n MOVE 0, R2\n PUSH R4\n RET\nMD_RET XOR R6, 0, R6\n JP_Z MD_RET1\n XOR R2, -1, R2");
		Analyzator.codeFRISC.append("\n ADD R2, 1, R2\nMD_RET1 POP R4\n PUSH R2\n PUSH R3\n PUSH R4\n RET\nMUL CALL MD_INIT");
		Analyzator.codeFRISC.append("\n XOR R1, 0, R1\n JP_Z MUL_RET\n SUB R1, 1, R1\nMUL_1 ADD R2, R0, R2");
		Analyzator.codeFRISC.append("\n SUB R1, 1, R1\n JP_NN MUL_1\nMUL_RET CALL MD_RET\n RET\nDIV CALL MD_INIT");
		Analyzator.codeFRISC.append("\n XOR R1, 0, R1\n JP_Z DIV_RET\nDIV_1 ADD R2, 1, R2\n SUB R0, R1, R0");
		Analyzator.codeFRISC.append("\n JP_NN DIV_1\n SUB R2, 1, R2\nDIV_RET CALL MD_RET\n RET");
		Analyzator.codeFRISC.append("\nMOD PUSH R1\n LOAD R1, (R7 + %D 8)\n LOAD R6, (R7 + %D 12)\nMOD_DO CMP R6, R1");
		Analyzator.codeFRISC.append("\n JP_SLT MOD_END\n SUB R6, R1, R6\n JP MOD_DO\nMOD_END POP R1\n RET");
	}
}
