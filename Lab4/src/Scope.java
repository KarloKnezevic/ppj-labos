import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



/**
 * Klasa za djelokrug
 * NOTE: Koristiti konstruktor koji prima parametar Scope parent!
 * Konstruktor bez parametara se koristi samo jednom i stvara instancu globalnog djelokruga!
 * @author wex
 *
 */
public class Scope {
	
	private Scope parent;
	private List<String> variables;
	private List<Scope> children;
	private List<String> arrays;
	
	private static int nextID = 0;
	public int ID;
	
	public Scope(){
		parent = null;
		variables = new ArrayList<String>();
		children = new ArrayList<Scope>();
		arrays = new ArrayList<String>();
		
		ID = nextID++;
	}
	
	/**
	 * Ovom konstruktoru predaj trenutni aktivni djelokrug, taj trenutni aktivni djelokrug ce biti parent ovom novom.
	 * Predvidena uporaba:
	 * 
	 * currentScope = new Scope(currentScope);
	 * 
	 * Ovo ce stvoriti novi djelokrug kojem ce roditelj biti trenutni djelokrug, 
	 * te jos postavlja trenutni djelokrug na taj novi.
	 * @param parent
	 */
	public Scope(Scope parent){
		this.parent = parent;
		children = new ArrayList<Scope>();
		parent.children.add(this);
		variables = new ArrayList<String>();
		arrays = new ArrayList<String>();
		
		ID = nextID++;
	}
	
	public String addVariable(String name){
		variables.add(name);
		return name + "_" + Integer.valueOf(ID).toString();
	}
	
	public String addArray(String name, int size){
		arrays.add(name);
		variables.add(name);
		for(int i = 0; i < size; i++)
			variables.add(name + Integer.valueOf(i).toString());
		return name + "_" + Integer.valueOf(ID).toString();
	}
	
	public boolean isArray(String name){
		return arrays.contains(name);
	}
	
	public String findLabel(String name){
		if(ID == 0)
			return null;
		if(variables.contains(name))
			return name + "_" + Integer.valueOf(ID).toString();
		return parent.findLabel(name);
	}
	
	public Scope getParent(){
		return parent;
	}
}
