

import java.util.*;

/**
 * Klasa za pravila leksickog analizatora, sadrzi atribute za spremanje u kojem
 * stanju LA mora biti da bi pravilo bilo aktivno, navedeni regex koji se mora
 * detektirati (jos uvijek nisam siguran kako izvesti ono ako nije regex koji je
 * prije definiran) dalje sprema
 * 
 * @author wex
 *
 */
public class Pravilo {
	private String stanjeLA;
	private E_NKA enka;
	private String lex_jedinka;
	private List<String> posebni_argumenti;
	private List<String> svi_argumenti;

	/**
	 * Prvi parametar je stanjeLA u kojem treba biti LA da bi ovo pravilo bilo
	 * aktivno.
	 * 
	 * Drugi parametar je automat koji provjerava jel trenutni ulazni niz
	 * regularni izraz kojeg zahtjeva ovo pravilo.
	 * 
	 * Treci parametar su svi argumenti u tijelu pravila spremljeni u listu
	 * stringova, konstruktor ce uzeti prvi (indeks 0) element s liste i
	 * spremiti ga kao prepoznatu leksicku jedinku (procitajte format tih
	 * pravila u upute.pdf), ostali elementi s liste se spremaju kao posebni
	 * argumenti (NOVI_RED etc...)
	 * 
	 * @param stanjeLA
	 * @param regex
	 * @param argumenti
	 */
	public Pravilo(String stanjeLA, E_NKA enka, List<String> argumenti) {
		this.stanjeLA = stanjeLA;
		this.enka = enka;
		this.lex_jedinka = argumenti.get(0);
		this.svi_argumenti = new ArrayList<String>();
		this.svi_argumenti.addAll(argumenti);
		argumenti.remove(0);
		if (argumenti.size() == 0)
			posebni_argumenti = null;
		else
			this.posebni_argumenti = argumenti;
	}

	/**
	 * Metoda izvodi ovo pravilo, one posebne argumente NOVI_REDAK,
	 * UDJI_U_STANJE, VRATI_SE te, ako je to argument ovog pravila, stvara
	 * leksicku jedinku i stavlja ju u tablicu.
	 * 
	 * Metoda mijenja public static varijable LA!
	 */
	public void izvedi() {
		Lex_jedinka nova_lex_jedinka = null;
		if (!lex_jedinka.equals("-")) {
			nova_lex_jedinka = new Lex_jedinka(lex_jedinka, LA.br_redaka, "");
		}

		boolean ima_VRATI_SE = false;

		if(posebni_argumenti != null){ //u slucaju da nema nijednog posebnog argumenta
			for (String argument : posebni_argumenti) {
				String[] argument_split = argument.split(" ");
				if (argument_split[0].equals("NOVI_REDAK"))
					LA.br_redaka++;
				if (argument_split[0].equals("UDJI_U_STANJE"))
					LA.stanje = argument_split[1];
				else if (argument_split[0].equals("VRATI_SE")) {
					ima_VRATI_SE = true;
					String simboli = "";
					int brSimbola = Integer.parseInt(argument_split[1]);
					while (brSimbola > 0) {
						simboli += LA.source[LA.pocetak++];
						brSimbola--;
					}
					if(nova_lex_jedinka != null){
						nova_lex_jedinka.setSimboli(simboli);
						LA.tablica_lex_jedinki.add(nova_lex_jedinka);
					}
					LA.trenutno = LA.zadnje = LA.pocetak;
					LA.zadnje--;
				}
			}
		}
		
		if (!ima_VRATI_SE) {
			if(nova_lex_jedinka != null){
				String simboli = "";
				for (int i = LA.pocetak; i <= LA.zadnje; i++)
					simboli += LA.source[i];
				nova_lex_jedinka.setSimboli(simboli);
				LA.tablica_lex_jedinki.add(nova_lex_jedinka);
			}
			LA.pocetak = LA.trenutno = LA.zadnje = LA.zadnje + 1;
			LA.zadnje--;
		}
	}

	/**
	 * Getter za podatak u kojem stanju treba biti LA da bi ovo pravilo bilo
	 * aktivno.
	 * 
	 * @return
	 */
	public String getStanjeLA() {
		return this.stanjeLA;
	}

	public String toString() {
		String args = "";
		for (String arg : svi_argumenti) {
			args += arg + "##";
		}
		String aaa = this.stanjeLA + "%%" + this.enka.toString() + "%%" + args.substring(0, args.length() - 2);
		return aaa;
	}
	
	public void inicirajAutomat(){
		enka.init();
	}
	
	public boolean enkaStanjaPrazna(){
		return enka.stanjaPrazna();
	}
	
	public boolean enkaUPrihvatljivom(){
		return enka.uPrihvatljivom();
	}
	
	public void enkaRadiPrijelaz(char uz){
		enka.radiPrijelaz(uz);
	}

}
