

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.events.Characters;

/**
 * Leksicki analizator:
 * Ima listu epsilon-NKA-ova kao atribut, 
 * pojedini epsilon-NKA provjerava je li dio niza koji 
 * se trenutno analizira jedan od regularnih izraza koji su
 * definirani na pocetku pravila jezika u "input datoteci" za GLA
 * 
 * Prijelazi epsilon-NKA-ova itd bi trebali biti generirani
 * od strane GLA!
 * @author wex
 *
 */
public class LA {

	private static List<E_NKA> enkas = new ArrayList<E_NKA>(); //ne koristi se, vezemo automate za pravila
	private static List<String> stanja = new ArrayList<String>(); //bitno nam je samo pocetno stanje...
	private static List<String> imena_lex_jedinki = new ArrayList<String>(); //zapravo je ovo nebitno
	private static List<Grupa_pravila> grupe_pravila = new ArrayList<Grupa_pravila>();
	
	public static int br_redaka = 1;
	public static String stanje;
	public static List<Lex_jedinka> tablica_lex_jedinki = new ArrayList<Lex_jedinka>();
	public static int pocetak = 0, trenutno = 0, zadnje = 0;
	/**
	 * Objasnjenje indeksa 'pocetak', 'trenutno' i 'zadnje':
	 * 'pocetak' -> pokazuje na prvi simbol source-a koji jos nije analiziran
	 * 
	 * 'trenutno' -> pokazuje na trenutni simbol koji se analizira
	 * 
	 * 'zadnje' -> pokazuje na zadnji simbol neanaliziranog niza tako da je niz od
	 * 'pocetak' do 'zadnje' neka prepoznata leksicka jedinka
	 * N.B.: NISAM SIGURAN JEL NAM UOPCE INDEKS 'zadnje' TREBA ->
	 * jer neka od pravila ce imati posebni argument VRATI_SE koji grupira
	 * prvih n znakova od neanaliziranog niza u lex jedinku...
	 */
	public static char[] source;
	
	public static class Grupa_pravila{
		public String stanjeLA;
		public List<Pravilo> pravila;
		
		public Grupa_pravila(String stanjeLA){
			this.stanjeLA = stanjeLA;
			pravila = new ArrayList<Pravilo>();
		}
	}
	
	public static void inicirajAutomateIzGrupe(Grupa_pravila grupa){
		if(grupa != null)
			for(Pravilo pravilo : grupa.pravila)
				pravilo.inicirajAutomat();
		
		else
			for(Grupa_pravila grupa2 : grupe_pravila)
				for(Pravilo pravilo : grupa2.pravila)
					pravilo.inicirajAutomat();
	}
	
	public static Grupa_pravila vratiAktivnuGrupuPravila(){
		for(Grupa_pravila grupa : grupe_pravila){
			if(grupa.stanjeLA.equals(stanje))
				return grupa;
		}
		System.out.println("OVO SE NE BI TREBALO DOGODITI -> metoda vratiAktivnuGrupuPravila()");
		return null; //ne bi se trebalo dogoditi
	}
	
	public static boolean skupStanjaAutomataNijePrazan(Grupa_pravila grupa){
		for(Pravilo pravilo : grupa.pravila)
			if(!pravilo.enkaStanjaPrazna())
				return true;
		return false;
	}
	
	public static Pravilo dohvatiPrviAutomatUPrihvatljivomStanju(Grupa_pravila grupa){
		for(Pravilo pravilo : grupa.pravila)
			if(pravilo.enkaUPrihvatljivom())
				return pravilo;
		return null;
	}
	
	public static void radiPrijelazZaSveAutomateIzGrupe(Grupa_pravila grupa, char uz){
		for(Pravilo pravilo : grupa.pravila)
			pravilo.enkaRadiPrijelaz(uz);
	}
	
	public static boolean nekiAutomatUPrihvatljivomStanju(Grupa_pravila grupa){
		for(Pravilo pravilo : grupa.pravila)
			if(pravilo.enkaUPrihvatljivom())
				return true;
		return false;
	}
	
	public static void main(String[] args) throws Exception {
		/**
		 * GLA je upisao u .txt datoteku podatke o pocetnom stanju LA i
		 * sve podatke o pravilima LA koje LA na pocetku main metode
		 * ucitava, parsira i kreaira instance klasa koje su potrebne za
		 * leksicku analizu source koda.
		 * 
		 */
		BufferedReader fileReader = new BufferedReader(new FileReader("./izlaz.txt"));
		
		stanje = fileReader.readLine();
		
		/*
		 * Format pravila:
		 * <imeStanja>%%prijelazi automata odvojeni s dva znaka & u formatu
		 * ts,,uz->[nss]%%argumenti odvojeni s dva znaka # primjer:
		 * 
		 * <S_pocetno>%%2,,]->[3]N&&0,,$->[2]D&&3,,$->[1]D%%D_UGL_ZAGRADA
		 * 	ovaj 'D' ili 'N' predstavlja jel se radi o epsilon prijelazu ili ne
		 */
		StringBuilder wholeFile = new StringBuilder();
		int readInt = fileReader.read();
		while(readInt != -1){
			wholeFile.append((char) readInt);
			readInt = fileReader.read();
		}
		String[] parts = wholeFile.toString().split("&#&#");
		//UCITAVANJE PRAVILA
		for(String line : parts){
				String[] line_split = line.split("%%");
				String stanje = line_split[0].substring(1, line_split[0].length() - 1);
				
				String[] prijelazi = line_split[1].split("&&");
				E_NKA automat = new E_NKA("nebitno", "0", "1");
				
				for(String prijelaz : prijelazi){
						String ts = prijelaz.substring(0, prijelaz.indexOf(",,"));
						char uz = prijelaz.toCharArray()[prijelaz.indexOf(",,")+2];
						String nss = prijelaz.substring(prijelaz.indexOf("->")+3, prijelaz.length()-2);
						if(prijelaz.substring(prijelaz.length()-1, prijelaz.length()).equals("N"))
							automat.addPrijelaz(ts, uz, nss);
						else
							automat.addEPrijelaz(ts, nss);
				}
				
				String[] argumenti = line_split[2].split("##");
				List<String> argumentiList = new ArrayList<String>(argumenti.length);
				for(String arg : argumenti)
					argumentiList.add(arg);
				
				Pravilo novo_pravilo = new Pravilo(stanje, automat, argumentiList);
				boolean nasao = false;
				for(Grupa_pravila grupa : grupe_pravila){
					if(grupa.stanjeLA.equals(stanje)){
						grupa.pravila.add(novo_pravilo);
						nasao = true;
						break;
					}
				}
				if(!nasao){
					Grupa_pravila nova_grupa = new Grupa_pravila(stanje);
					nova_grupa.pravila.add(novo_pravilo);
					grupe_pravila.add(nova_grupa);
				}
		}
		
		
		
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		String sourceStringed = "";
		String line;
		
		//CITANJE SOURCE-A
		while((line = reader.readLine()) != null && !line.equals("EOF")) //uh.. tu se javlja infinite loop
			sourceStringed += line + "\n"; //valjda nece kad oni redirectaju file na stdin?
		reader.close();
		
		source = sourceStringed.toCharArray();
		
		//ANALIZIRANJE SOURCE-A
		Pravilo aktivno_pravilo = null;
		inicirajAutomateIzGrupe(null);
		int failsafeCount = 0;
		boolean endSource = false;
		while(trenutno <= source.length){ //PAZITI TU DA SE NE ZABORAVI NA ZADNJIH PAR SIMBOLA U SOURCEU!!!!
			Grupa_pravila grupa = vratiAktivnuGrupuPravila();
			Pravilo novo_pravilo = null;
			if(skupStanjaAutomataNijePrazan(grupa) && !endSource){ // && trenutno < source.length
				if((novo_pravilo = dohvatiPrviAutomatUPrihvatljivomStanju(grupa)) != null){
					aktivno_pravilo = novo_pravilo;
					zadnje = trenutno-1;
				}
				if(trenutno < source.length)
					radiPrijelazZaSveAutomateIzGrupe(grupa, source[trenutno++]);
				else
					endSource = true;
			}
			
			else{
				if(aktivno_pravilo == null){
					zadnje = pocetak;
					trenutno = pocetak = pocetak + 1;
				}
				else{
					aktivno_pravilo.izvedi();
					aktivno_pravilo = null;
				}
				inicirajAutomateIzGrupe(grupa);
				if(trenutno == source.length)
					break;
			}
			
		}
		//PRINTANJE TABLICE
		int size = tablica_lex_jedinki.size();
		for(int i = 0; i < size; i++){
			if(i < size-1)
				System.out.println(tablica_lex_jedinki.get(i));
			else
				System.out.print(tablica_lex_jedinki.get(i));
		}
			
	}
}
