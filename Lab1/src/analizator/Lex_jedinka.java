

public class Lex_jedinka {
	private String naziv;
	private int redak;
	private String simboli;
	
	public Lex_jedinka(String naziv, int redak, String simboli){
		this.naziv = naziv;
		this.redak = redak;
		this.simboli = simboli;
	}
	
	@Override
	public String toString(){
		return naziv + " " + Integer.toString(redak) + " " + simboli;
	}
	
	public void setSimboli(String simboli){
		this.simboli = simboli;
	}
}
