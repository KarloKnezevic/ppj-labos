


import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;


/**
 * Epsilon NKA, klasa sadrzi metode za dodavanje prijelaza (trenutno_stanje,
 * ulazni_znak -> novi_skup_stanja) za prijelaze je napravljena klasa Prijelaz
 * unutar klase E_NKA za laksi pristup komponentama prijelaza (trenutno_stanje
 * itd.)
 * 
 * @author wex
 *
 */
public class E_NKA {

	private String regex; // regex koji ovaj automat prihvaca IME REGEX-A!!
	private Set<Prijelaz> prijelazi;
	private String poc_stanje;
	private String[] prihvatljiva_stanja;
	private Set<String> trenutna_stanja;
	private int br_stanja;

	/**
	 * Konstruktor za enka, parametar "prihvatljiva_stanja" mora biti u obliku
	 * "stanje1,stanje2,stanje3,stanje4" itd
	 * 
	 * @param poc_stanje
	 * @param prihvatljiva_stanja
	 */
	public E_NKA(String regex, String poc_stanje, String prihvatljiva_stanja) {
		this.regex = regex;
		this.trenutna_stanja = new LinkedHashSet<String>();
		prijelazi = new LinkedHashSet<Prijelaz>();
		this.poc_stanje = poc_stanje;
		this.prihvatljiva_stanja = prihvatljiva_stanja.split(",");
		this.br_stanja = 0;

	}

	/**
	 * Novi skup stanja mora biti String oblika
	 * "stanje1,stanje2,stanje3,stanje4" itd
	 * 
	 * @param trenutno
	 *            stanje
	 * @param ulazni
	 *            znak
	 * @param novi
	 *            skup stanja
	 */
	public void addPrijelaz(String ts, char uz, String nss) {
		prijelazi.add(new Prijelaz(ts, uz, nss, false));
	}
	

	public void addEPrijelaz(String ts, String nss){
		prijelazi.add(new Prijelaz(ts, '$', nss, true));
	}

	/**
	 * Getter za regex koji ovaj automat prihvaca.
	 * 
	 * @return
	 */
	public String getRegex() {
		return regex;
	}

	/**
	 * Metoda inicira enka na epsilon-okruzje pocetnog stanja enka (logicno,
	 * sluzi i resetiranju enka) STARA METODA NE KORISTI SE!
	 */
	/*
	 * public void init(){ trenutna_stanja.clear();
	 * trenutna_stanja.add(poc_stanje);
	 * 
	 * this.doEpsilonPrijelaz(); }
	 */

	/**
	 * Metoda ce napraviti prijelaz za
	 * ovaj enka na temelju njegovih trenutnih stanja i predanog parametra koji
	 * predstavlja ulazni znak.
	 * 
	 * @param ulazni znak
	 * @return prihvatljivo stanje (true) ili ne (false)
	 */
	public void radiPrijelaz(char uz) {
		Set<String> nova_stanja = new LinkedHashSet<String>();
		
		for(String stanje : trenutna_stanja){
			for(Prijelaz prijelaz : prijelazi){
				if(prijelaz.ts.equals(stanje) && prijelaz.uz == uz && !prijelaz.jeEpsilonPrijelaz){
					for(String novo_stanje : prijelaz.nss){
						nova_stanja.add(novo_stanje);
					}
				}
			}
		}
		trenutna_stanja = nova_stanja;
		doEpsilonPrijelaz();
	}

	/**
	 * Metoda ce napraviti epsilon prijelaze za ovaj enka. Metoda vraca 'true'
	 * ako je enka u prihvatljivom stanju nakon izvodenja prijelaza, u suprotnom
	 * vraca 'false'
	 * 
	 * @return prihvatljivo stanje (true) ili ne (false)
	 */
	public boolean doEpsilonPrijelaz() {
		boolean changed = true;
		Set<String> novaStanja = new LinkedHashSet<String>();

		/*
		 * da znam da bi se ovaj dio pod ovom "while" petljom mogao puno
		 * efikasnije rijesiti al stari, onak, ne da mi se
		 */

		while (changed) {
			novaStanja = new LinkedHashSet<String>();
			novaStanja.addAll(trenutna_stanja);
			changed = false;
			for (String stanje : trenutna_stanja) {
				for (Prijelaz prijelaz : prijelazi) {
					if (prijelaz.ts.equals(stanje) && prijelaz.jeEpsilonPrijelaz) {
						novaStanja.addAll(Arrays.asList(prijelaz.nss));
					}
				}
			}
			changed = trenutna_stanja.addAll(novaStanja);
		}

		for (String stanje : trenutna_stanja) {
			if (Arrays.asList(prihvatljiva_stanja).contains(stanje))
				return true;
		}
		return false;
	}

	/**
	 * Nakon sto se kreira automat iz regexa, on ima jedno pocetno i jedno
	 * privatljivo stanje Da se ne zezamo sa dva poziva metoda dodaj stanje,
	 * objedinjeno je u jednu.
	 * 
	 * @param lijevo
	 * @param desno
	 */
	public void dodajPocetnoIPrihvatiljivoStanje(String lijevo, String desno) {
		this.poc_stanje = lijevo;
		this.trenutna_stanja.add(lijevo);
		this.prihvatljiva_stanja = new String[1];
		this.prihvatljiva_stanja[0] = desno;

	}

	/**
	 * Indeksira stanja prilikom kreiranja automata.
	 * 
	 * @return
	 */
	public String novo_stanje() {
		this.br_stanja = this.br_stanja + 1;
		return Integer.toString(br_stanja - 1);
	}

	public String toString() {
		String returnValue = "";
		for (Prijelaz pr : prijelazi) {
			returnValue += pr.toString() + "&&";
		}
		return returnValue.substring(0, returnValue.length() - 2);
	}
	
	public void init(){
		trenutna_stanja.clear();
		trenutna_stanja.add("0");
		doEpsilonPrijelaz();
	}
	
	public boolean stanjaPrazna(){
		return trenutna_stanja.isEmpty();
	}
	
	public boolean uPrihvatljivom(){
		return trenutna_stanja.contains("1");
	}

	private class Prijelaz {
		private String ts; // Trenutno Stanje
		private char uz; // Ulazni Znak
		private String[] nss; // Novi Skup Stanja
		private boolean jeEpsilonPrijelaz;

		/**
		 * Novi skup stanja mora biti String oblika
		 * "stanje1,stanje2,stanje3,stanje4" itd
		 * 
		 * @param trenutno
		 *            stanje
		 * @param ulazni
		 *            znak
		 * @param novi
		 *            skup stanja
		 */
		public Prijelaz(String ts, char uz, String nss, boolean isEpsilon) {
			this.ts = ts;
			this.uz = uz;
			this.nss = nss.split(",");
			jeEpsilonPrijelaz = isEpsilon;
		}

		/**
		 * ts,,uz->[nss] Za stanje ts, uz procitani znak uz, prijedji u skup
		 * stanja nss
		 */
		public String toString() {
			return this.ts + ",," + Character.toString(uz) + "->" + Arrays.toString(nss);
		}

		public boolean equals(Object other) {
			Prijelaz o = (Prijelaz) other;
			return o.toString().equals(this.toString());
		}
	}

}
