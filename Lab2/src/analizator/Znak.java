
public class Znak extends Cvor {
	private int row;
	private String lex_jedinka;

	public Znak(String line) {
		String[] parts = line.split(" ", 3);
		this.znak = parts[0];
		this.row = Integer.parseInt(parts[1]);
		this.lex_jedinka = parts[2];
	}

	@Override
	public String toStringRec(int depth) {
		String string = "";
		for (int i = 0; i < depth; i++)
			string += " ";
		string += znak + " " + Integer.toString(row) + " " + lex_jedinka + "\n";

		return string;
	}
	
	public int getRow(){
		return row;
	}
}
