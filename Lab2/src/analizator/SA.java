
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Stack;


public class SA {
	

	public static List<Znak> niz_znakova = new ArrayList<Znak>();
	public static int index = 0;
	public static HashMap<Integer, Akcija> akcije = new HashMap<Integer, Akcija>();
	public static HashMap<Integer, String> nova_stanja = new HashMap<Integer, String>();
	public static Stack<Cvor> stack = new Stack<Cvor>(); // stog za znakove gramatike
	public static Stack<String> stanja_stack = new Stack<String>(); // stog za stanja...mozda ce biti problema...
	public static boolean prihvacen = false;
	public static HashMap<Integer, String> sync_znakovi = new HashMap<Integer, String>();
	
	/*
	 * U hashmapama keyevi ce biti stringovi tako da se konkatenira string stanje u kojem treba
	 * biti parser + 1 whitespace " " + string koji ulazni znak treba biti
	 */
	
	public static void oporavak(){
		System.err.println("Doslo je do pogreske u redu: " + Integer.toString(niz_znakova.get(index).getRow())
				+ "   Odbacujem znakove: ");
		
		if(index < niz_znakova.size()){
			while(!sync_znakovi.containsKey(niz_znakova.get(index).getZnak().hashCode()) && index < niz_znakova.size()){
				System.err.print(niz_znakova.get(index).toStringRec(0));
				index++;
			}
		}
		
		if(index >= niz_znakova.size()){
			while(!akcije.containsKey((stanja_stack.peek() + " \27").hashCode())){
				stanja_stack.pop();
				stack.pop();
			}
		}
		else{
			while(!akcije.containsKey((stanja_stack.peek() + " " + niz_znakova.get(index).getZnak()).hashCode())){
				stanja_stack.pop();
				stack.pop();
			}
		}
	}
	
	
	/**
	 * @param args
	 * @throws Exception 
	 */
	public static void main(String[] args) throws Exception {
		/*
		 * Sta mi sve treba od GSA, trebaju mi sve valjane akcije i tu mi treba:
		 *  ->jel se radi o Pomakni ili Reduciraj akciji
		 *  ->u kojem stanju treba biti parser i koji znak treba biti na ulazu da bi se akcija izvrsila
		 *  	(ako sam u nekom paru (stanje, ulazni znak) za koji nema definirane akcije onda 
		 *  	pretpostavljam da je akcija 'Odbaci' tj. odbacujem sve do prvog sync znaka)
		 *  ->koje je sljedece stanje parsera nakon izvrsenja akcije
		 *  ->ako se radi o Reduciraj akciji treba mi cijela redukcija koja se izvrsava
		 * 
		 * Tablica NovoStanje opet, za trenutno stanje i trenutni znak na vrhu stoga u koje stanje ide parser
		 * 
		 * Popis sinkronizacijskih znakova
		 */
		
		BufferedReader fileReader = new BufferedReader(new FileReader("izlaz.txt"));
		stanja_stack.push(fileReader.readLine()); //pocetno stanje je u prvom redu
		String line;
		
		while(!(line = fileReader.readLine()).equals("=====")){
			String[] parts1 = line.split("###"); //parts1[0] je stanje/red u tablicama cija polja sad popunjavam
			if(parts1.length < 2) continue;
			String[] akcije = parts1[1].split("&&&");
			for(String akcija : akcije){
				String[] akcijaParts = akcija.split("%%%"); //akcijaParts[0] je ulazni znak / stupac tablice
				if(akcijaParts[1].startsWith("p"))
					SA.akcije.put((parts1[0] + " " + akcijaParts[0]).hashCode(), new Akcija(parts1[0], akcijaParts[0], akcijaParts[1].substring(1), "pomakni"));
				else if(akcijaParts[1].startsWith("r"))
					SA.akcije.put((parts1[0] + " " + akcijaParts[0]).hashCode(), new Akcija(parts1[0], akcijaParts[0], akcijaParts[1].substring(2, akcijaParts[1].length()-1), "reduciraj"));
				else if(akcijaParts[1].startsWith("a"))
					SA.akcije.put((parts1[0] + " " + akcijaParts[0]).hashCode(), new Akcija(parts1[0], akcijaParts[0], "", "prihvati"));
			}
			
			if(parts1.length < 3) continue;
			String[] novaStanja = parts1[2].split("&&&");
			for(String novoStanje : novaStanja){
				String[] novoStanjeParts = novoStanje.split("%%%"); //novoStanjeParts[0] je ulazni znak / stupac tablice
				SA.nova_stanja.put((parts1[0] + " " + novoStanjeParts[0]).hashCode(), novoStanjeParts[1]);
			}
		}
		line = fileReader.readLine();
		String[] sync = line.split(" ");
		for(int i = 0; i < sync.length; i++)
			sync_znakovi.put(sync[i].hashCode(), sync[i]);
		
		fileReader.close();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		
		while((line = reader.readLine()) != null && !line.equals("EOF"))
			niz_znakova.add(new Znak(line));
		
		while(!prihvacen){
			Akcija akcija;
			if(index < niz_znakova.size())
				akcija = akcije.get((stanja_stack.peek() + " " + niz_znakova.get(index).getZnak()).hashCode());
			else
				akcija = akcije.get((stanja_stack.peek() + " \27").hashCode());
			
			if(akcija != null)
				akcija.izvrsi();
			else
				oporavak();
		}
		System.out.print(stack.pop().toStringRec(0));
	}
}
