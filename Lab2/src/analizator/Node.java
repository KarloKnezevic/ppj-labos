

import java.util.*;

public class Node {
	protected String znak;
	private List<Node> children;
	
	protected Node(){}
	
	public Node(String znak){
		this.znak = znak;
		children = new ArrayList<Node>();
	}
	
	public void addChild(Node child){
		children.add(child);
	}
	
	public String getZnak(){
		return znak;
	}
	
	public String toStringRec(int depth){
		String string = "";
		for(int i = 0; i < depth; i++)
			string += " ";
		string += znak + "\n";
		
		for(int i = 0; i < children.size(); i++)
			string += children.get(i).toStringRec(depth+1);
		return string;
	}
}
