
import java.util.ArrayList;
import java.util.List;

public class Cvor {
	protected String znak;
	private List<Cvor> children;

	protected Cvor() {
	}

	public Cvor(String znak) {
		this.znak = znak;
		children = new ArrayList<Cvor>();
	}

	public void addChild(Cvor child) {
		children.add(child);
	}

	public String getZnak() {
		return znak;
	}

	public String toStringRec(int depth) {
		String string = "";
		for (int i = 0; i < depth; i++)
			string += " ";
		string += znak + "\n";

		if(children.size() > 0){
			for (int i = 0; i < children.size(); i++)
				string += children.get(i).toStringRec(depth + 1);
		}
		else{
			for(int i = 0; i < depth+1; i++)
				string += " ";
			string += "$\n";
		}
		return string;
	}
}
