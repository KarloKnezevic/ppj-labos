import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Klasa koja modelira jedno stanje te sadrzi njegove prijelaze za odreden znak.
 * @author Murta
 *
 */
public  class Stanje{
	public LRStavka stanje;
	public Map<String, LinkedList<LRStavka>> prijelazi = new LinkedHashMap<String, LinkedList<LRStavka>>();		
	
	public Stanje(LRStavka stanje){
		this.stanje = stanje;
	}
	/**
	 * Metoda koja dodaje stanja koja se prelazi epsilonom za odredeni znak: UTR 37.str, 4)
	 * Alogritam je moj sa UTR-a za epsilon prijelaze, trebao bi raditi.
	 * @param znak
	 */
	public void prebaciEpsilon(String znak, LinkedHashSet<Stanje> stanja,
			LinkedHashMap<LRStavka, Stanje> stavkaStanje) {
		Set<LRStavka> sveNoveStavke = new LinkedHashSet<LRStavka>();
		Set<LRStavka> cijelaEpsilonOkolina = new LinkedHashSet<LRStavka>();
		cijelaEpsilonOkolina = this.epsilonOkruzenje(stavkaStanje);
		
		Set<Stanje> stanjaOvogKoraka = new LinkedHashSet<Stanje>();
		stanjaOvogKoraka.clear();
		
		for(Stanje stanje : stanja){
			if(cijelaEpsilonOkolina.contains(stanje.stanje)){
				stanjaOvogKoraka.add(stanje);
			}
		}
		stanjaOvogKoraka.add(this);
		
		for(Stanje stanje : stanjaOvogKoraka){
			if(stanje.prijelazi.containsKey(znak)){
				for(LRStavka stavka : stanje.prijelazi.get(znak)){
					sveNoveStavke.add(stavka);
				}
			}
		}
		if(prijelazi.get(znak)!=null){
			sveNoveStavke.addAll(prijelazi.get(znak));
		}
		
		for(LRStavka stavka : sveNoveStavke){
			sveNoveStavke.addAll(stavkaStanje.get(stavka).epsilonOkruzenje(stavkaStanje));
		}
		
		prijelazi.put(znak, new LinkedList<LRStavka>());
		prijelazi.get(znak).addAll(sveNoveStavke);
	}
	
	public Set<LRStavka> epsilonOkruzenje(LinkedHashMap<LRStavka, Stanje> stavkaStanje){
		
		Set<LRStavka> cijelaEpsilonOkolina = new LinkedHashSet<LRStavka>();
		cijelaEpsilonOkolina.addAll(prijelazi.get(""));
		HashSet<LRStavka> temp = new LinkedHashSet<>(cijelaEpsilonOkolina);
		
		Set<Stanje> stanjaOvogKoraka = new LinkedHashSet<Stanje>();
		Set<LRStavka> stavkePrijeKoraka = new LinkedHashSet<>();
		//ulazi u petlju u kojoj provjerava je li ostalo stanja koja
		//bi mogla imati epsilon prijelaze koje nismo naveli
		while(!temp.isEmpty()){
			stavkePrijeKoraka.addAll(cijelaEpsilonOkolina);
			stanjaOvogKoraka.clear();
			for(LRStavka stavka : temp){
				stanjaOvogKoraka.add(stavkaStanje.get(stavka));
			}
			for(Stanje stanje : stanjaOvogKoraka){
				if(stanje.prijelazi.containsKey("")){
					//List<LRStavka> lista = stanje.prijelazi.get("");
					cijelaEpsilonOkolina.addAll(stanje.prijelazi.get(""));
				}
			}
			temp.clear();
			temp.addAll(cijelaEpsilonOkolina);
			temp.removeAll(stavkePrijeKoraka);
		}
		return cijelaEpsilonOkolina;
	}
	
	/**
	 * Metoda dodavanja prijelaza. Sve je jasno, samo si vizualno hocu odvojiti kod.
	 * @param znak
	 * @param novo
	 */
	public void dodajPrijelaz(String znak, LRStavka novo){
		if(prijelazi.containsKey(znak)){
			if(!prijelazi.get(znak).contains(novo)){
				prijelazi.get(znak).add(novo);
			}
		}
		else{
			prijelazi.put(znak, new LinkedList<LRStavka>());
			prijelazi.get(znak).add(novo);
		}
	}
	
	public void prijelazUPrazanSkup(String znak) {
		prijelazi.put(znak, new LinkedList<LRStavka>());
	}

	@Override
	public int hashCode() {
		return stanje.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Stanje other = (Stanje) obj;
		if (stanje == null) {
			if (other.stanje != null)
				return false;
		} else if (!stanje.equals(other.stanje))
			return false;
		return true;
	}
	
	@Override
	public String toString(){
		return stanje.toString();
	}
}
