/**
 * Modelira produkciju. Produkcija je odredjena svojoj ljevom i desnom stranom
 * kao i rednim brojem pod kojim se pojavila u ulaznoj datoteci
 * 
 * @author luka
 *
 */

public class Produkcija {

	public String lijevaStrana;
	public String desnaStrana;
	public int redniBroj;
	private int brojZnakovaDesneStrane;
	public String[] znakoviDesneStrane;

	public Produkcija(String lin, String desnaStrana, int redniBroj) {
		this.lijevaStrana = lin.trim();
		this.desnaStrana = desnaStrana.trim();
		this.redniBroj = redniBroj;

		// za lakse rukovanje stavljam sve znakove u array jedan
		if (this.desnaStrana.contains(" ")) {
			this.znakoviDesneStrane = this.desnaStrana.split(" ");
			this.brojZnakovaDesneStrane = this.znakoviDesneStrane.length;
		} else {
			this.znakoviDesneStrane = new String[] { this.desnaStrana };
			this.brojZnakovaDesneStrane = 1;
		}

		if (this.desnaStrana.equals("$")) {
			this.desnaStrana = "";
		}
	}

	/**
	 * Ispis u BNF notaciji. Nicem ne sluzi osim laksem debagiranju.
	 */
	public String toString() {
		return lijevaStrana + " ::= " + desnaStrana;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((desnaStrana == null) ? 0 : desnaStrana.hashCode());
		result = prime * result + ((lijevaStrana == null) ? 0 : lijevaStrana.hashCode());
		result = prime * result + redniBroj;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Produkcija other = (Produkcija) obj;
		if (desnaStrana == null) {
			if (other.desnaStrana != null)
				return false;
		} else if (!desnaStrana.equals(other.desnaStrana))
			return false;
		if (lijevaStrana == null) {
			if (other.lijevaStrana != null)
				return false;
		} else if (!lijevaStrana.equals(other.lijevaStrana))
			return false;
		if (redniBroj != other.redniBroj)
			return false;
		return true;
	}

	/**
	 * Isto nepotrebno osim za debagiranje.
	 */

}
