import java.util.Stack;



/**
 * Tu ide staticka metoda za pokretanje analize koja ce pozvati analizu na znak koji je korijen u genStablu.
 * Podijelit cemo metode kako su podijeljene u uputama
 * 		- izrazi (poglavlje 4.4.4.) klasa Expression
 * 		- naredbe (poglavlje 4.4.5.) klasa Command
 * 		- deklaracije/definicije (poglavlje 4.4.6.) klasa Declaration
 * Ove klase ce biti apstraktne i imat ce samo staticke metode, u pocetku bi bilo dobro da svatko pise
 * metode u jednu od tih klasa pa kasnije onda dovrsavamo zajedno ako netko zaostane.
 * Ako cemo pisati u istu klasu vise nas onda onaj koji merge-a samo mora pripaziti i sve ce bit oke.
 * 
 * U ovu klasu Analyzator stavljajte i podatkovne strukture koje ce biti potrebne za globalnu komunikaciju i slicno.
 * Kad god mozete saljite potrebne parametre preko argumenata poziva metoda.
 * Na svaku metodu napisite "@author ..."!
 * 
 * NOTE: Znam da je ova metoda "start" nepotrebna (jer postoji samo jedan pocetni nezavrsni znak 
 * gramatike pa ne treba ni provjeravati ovo sve sa if-ovima, al ovako barem vidimo sta je implementirano a sta ne)
 * Kasnije mozemo maknuti ovu metodu, tj ove sve if-ove i samo staviti da poziva direktno analizu pocetnog nezavrsnog znaka.
 * 
 * NOTE2: Svaka metoda mora imati parametar TreeNode, da "zna" gdje se trenutno nalazimo u generativnom stablu
 * i da zna o kojoj produkciji se radi!
 * @author wex
 *
 */
public abstract class Analyzator {
	//TODO nije implementirano tako da je T 'int' ili 'char' vec bilo koji tip -> mozda problem mozda ne?
	//Ovdje stavite ako su vam potrebne neke globalne podatkovne strukture
	
	public static Scope currentScope = new Scope(); //inicijalizacija pocetnog (globalnog) djelokruga
	public static Stack<Object> stack = new Stack<Object>();
	public static Stack<Object> funcParamsStack = new Stack<Object>();
	public static Stack<Object> inheritanceStack = new Stack<Object>();
	public static Stack<Object> functionCallStack = new Stack<Object>(); //jako glupo ovo sa stackovima...
	
	//Ovdje pocinju metode
	
	/**
	 * Metoda za ispis greske!
	 * Znaci kad u metodi nekoj zakljucite da je doslo do greske pozovete ovu
	 * metodu i predate joj node koji ste dobili kao parametar u toj metodi u kojoj se nalazite!
	 * @author wex
	 * @param node
	 */
	public static void printErr(TreeNode node){
		StringBuilder builder = new StringBuilder();
		
		builder.append(node.toString() + " ::=");
		
		for(int i = 0; i < node.getChildrenAmount(); i++)
			builder.append(" " + node.getChild(i).toString());
		
		System.out.println(builder.toString());
		System.exit(0);
	}
	
	
	/**
	 * Ulazna metoda u analizu, kao parametar joj se predaje korijen generativnog stabla
	 * Ova metoda je redundantna i bit ce promijenjena, al zasad nek bude tu cisto da vidimo
	 * koje metode za analizu su implementirane a koje ne.
	 * @author wex
	 * @param node
	 */
	public static void start(TreeNode node){
		if(node.getName().equals("<prijevodna_jedinica>"))
			Command.prijevodna_jedinica(node);
		
		else if(node.getName().equals("<postfiks_izraz>"))
			Expression.postfiks_izraz(node);
			
		else if(node.getName().equals("<lista_argumenata>"))
			Expression.lista_argumenata(node);
		
		else if(node.getName().equals("<unarni_izraz>"))
			Expression.unarni_izraz(node);
		
		else if(node.getName().equals("<cast_izraz>"))
			Expression.cast_izraz(node);
		
		else if(node.getName().equals("<ime_tipa>"))
			Expression.ime_tipa(node);
		
		else if(node.getName().equals("<multiplikativni_izraz>"))
			Expression.multiplikativni_izraz(node);
		
		else if(node.getName().equals("<aditivni_izraz>"))
			Expression.aditivni_izraz(node);
		
		else if(node.getName().equals("<odnosni_izraz>"))
			Expression.odnosni_izraz(node);
		
		else if(node.getName().equals("<jednakosni_izraz>"))
			Expression.jednakosni_izraz(node);
		
		else if(node.getName().equals("<bin_i_izraz>"))
			Expression.bin_i_izraz(node);
		
		else if(node.getName().equals("<bin_xili_izraz>"))
			Expression.bin_xili_izraz(node);
		
		else if(node.getName().equals("<bin_ili_izraz>"))
			Expression.bin_ili_izraz(node);
		
		else if(node.getName().equals("<log_i_izraz>"))
			Expression.log_i_izraz(node);
		
		else if(node.getName().equals("<log_ili_izraz>"))
			Expression.log_ili_izraz(node);
		
		else if(node.getName().equals("<izraz_pridruzivanja>"))
			Expression.izraz_pridruzivanja(node);
		
		else if(node.getName().equals("<izraz>"))
			Expression.izraz(node);
		
		else if(node.getName().equals("<slozena_naredba>"))
			Command.slozena_naredba(node);
		
		else if(node.getName().equals("<lista_naredbi>"))
			Command.lista_naredbi(node);
		
		else if(node.getName().equals("<naredba>"))
			Command.naredba(node);
		
		else if(node.getName().equals("<izraz_naredba>"))
			Command.izraz_naredba(node);
		
		else if(node.getName().equals("<naredba_grananja>"))
			Command.naredba_grananja(node);
		
		else if(node.getName().equals("<naredba_petlje>"))
			Command.naredba_petlje(node);
		
		else if(node.getName().equals("<naredba_skoka>"))
			Command.naredba_skoka(node);
		
		else if(node.getName().equals("<primarni_izraz>"))
			Expression.primarni_izraz(node);
		
		else if(node.getName().equals("<vanjska_deklaracija>"))
			Command.vanjska_deklaracija(node);
		
		else if(node.getName().equals("<defincija_funkcije>"))
			Declaration.definicija_funkcije(node);
		
		else if(node.getName().equals("<lista_parametara>"))
			Declaration.lista_parametara(node);
		
		else if(node.getName().equals("<deklaracija_parametara>"))
			Declaration.deklaracija_parametra(node);
		
		else if(node.getName().equals("<lista_deklaracija>"))
			Declaration.lista_deklaracija(node);
		
		else if(node.getName().equals("<deklaracija>"))
			Declaration.deklaracija(node);
		
		else if(node.getName().equals("<lista_init_deklaratora>"))
			Declaration.lista_init_deklaratora(node);
		
		else if(node.getName().equals("<init_deklarator>"))
			Declaration.init_deklarator(node);
		
		else if(node.getName().equals("<izravni_deklarator>"))
			Declaration.izravni_deklarator(node);
		
		else if(node.getName().equals("<inicijalizator>"))
			Declaration.inicijalizator(node);
		
		else if(node.getName().equals("<lista_izraza_pridruzivanja>"))
			Declaration.lista_izraza_pridruzivanja(node);
	}

}
